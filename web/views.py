from django.shortcuts import render
from django.http import JsonResponse
from json import JSONEncoder
from django.views.decorators.csrf import csrf_exempt
from web.models import User, Token, Expense, Income
from datetime import datetime
# Create your views here.

@csrf_exempt
def submit_expense(request):
    """ user submits a request an expense """
    this_token = request.POST['token']
    this_user = User.objects.filter(token__token = this_token).get()
    if 'date' not in request.POST:  
        now = datetime.now()
    
    user_expense = Expense(user = this_user, amount = request.POST['amount'],
            text = request.POST['text'], date = now)
    user_expense.save()
    print (request)
    return JsonResponse({
        'status': '200',

    }, encoder=JSONEncoder)

@csrf_exempt
def submit_income(request):
    """ user submits a request an income """
    this_token = request.POST['token']
    this_user = User.objects.filter(token__token = this_token).get()
    if 'date' not in request.POST:  
        now = datetime.now()
    
    user_income = Income(user = this_user, amount = request.POST['amount'],
            text = request.POST['text'], date = now)
    user_income.save()
    print (request)
    return JsonResponse({
        'status': '200',

    }, encoder=JSONEncoder)
